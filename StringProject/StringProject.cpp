#include <iostream>
#include <string>

int main()
{
    std::string my_string = "Hello, world!";
    std::cout << my_string << std::endl;
    std::cout << my_string.length() << std::endl;
    std::cout << my_string[0] << std::endl;
    std::cout << my_string[my_string.size() - 1] << std::endl;
    return 0;
}